'use strict';
var AWS = require('aws-sdk');
var dynamodb = new AWS.DynamoDB();
var route53 = new AWS.Route53();
var zoneid = HIDDEN;
var tld = '.taisun.io';
exports.handler = function(event, context, callback) {
  var sourceip = event.requestContext.identity.sourceIp;
  var responseCode = 200;
  if (event.queryStringParameters !== null && event.queryStringParameters !== undefined) {
    if (event.queryStringParameters.key !== undefined && event.queryStringParameters.key !== null && event.queryStringParameters.key !== "") {
      var dynamoparams = {
        Key: {
          "guid": {
            S: event.queryStringParameters.key
          }
        },
        TableName: "dyndns"
      };
      dynamodb.getItem(dynamoparams, function(err, data) {
        if (err) {
          console.log(err, err.stack);
        }
        else {
          if (data.Item == null) {
            var responseBody = {
              sourceip: sourceip,
              error: event.queryStringParameters.key + ' is not a valid domain dnskey'
            };
            respond(responseBody);
          }
          else {
            console.log(data.item);
            var ip = data.Item.ip.S;
            var dnskey = data.Item.guid.S;
            var subdomain = data.Item.domain.S;
            if (ip == sourceip) {
              var responseBody = {
                sourceip: sourceip,
                action: 'none',
                message: 'The subdomain ' + subdomain + '.taisun.io is allready set to ' + ip
              };
              respond(responseBody);
            }
            else {
              var params53 = {
                ChangeBatch: {
                  Changes: [{
                    Action: "UPSERT",
                    ResourceRecordSet: {
                      Name: subdomain + tld,
                      ResourceRecords: [{
                        Value: sourceip
                      }],
                      TTL: 60,
                      Type: "A"
                    }
                  }]
                },
                HostedZoneId: zoneid
              };
              route53.changeResourceRecordSets(params53, function(err, data) {
                if (err) {
                  console.log(err, err.stack);
                }
                else {
                  var dynupdateparams = {
                    TableName: "dyndns",
                    Key: {
                      "guid": {
                        S: event.queryStringParameters.key
                      }
                    },
                    UpdateExpression: "set ip = :ip",
                    ExpressionAttributeValues: {
                      ":ip": {
                        S: sourceip
                      }
                    },
                    ReturnValues: "UPDATED_NEW"
                  };
                  dynamodb.updateItem(dynupdateparams, function(err, data) {
                    if (err) {
                      console.log(err, err.stack);
                    }
                    else {
                      var responseBody = {
                        sourceip: sourceip,
                        action: 'updated',
                        message: 'The subdomain ' + subdomain + '.taisun.io is set to ' + sourceip + ' from ' + ip
                      };
                      respond(responseBody);
                    }
                  });
                }
              });
            }
          }
        }
      });
    }
    else {
      var responseBody = {
        sourceip: sourceip,
        action: 'none',
        message: 'Parameter passed not recognized this endpoint requires a ?key=<yourdnskey> parameter'
      };
      respond(responseBody);
    }
  }
  else {
    var responseBody = {
      sourceip: sourceip,
      action: 'none',
      message: 'No parameters passed this endpoint requires a ?key=<yourdnskey> parameter'
    };
    respond(responseBody);
  }

  function respond(responseBody) {
    console.log(responseBody);
    var response = {
      statusCode: responseCode,
      body: JSON.stringify(responseBody, null, 4)
    };
    callback(null, response);
  }
};
