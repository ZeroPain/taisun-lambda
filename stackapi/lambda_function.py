import json
import pymysql
from pymysql.constants import ER
import yaml
import base64
import uuid
import boto3
from botocore.exceptions import ClientError
from os import environ

def lambda_handler(event, context):
  # Main Mysql Connect
  db=pymysql.connect(
  host=environ.get('MYSQL_URL'),
  user=environ.get('MYSQL_USER'),
  port=3306,
  passwd=environ.get('MYSQL_PASS'),
  db=environ.get('MYSQL_DATABASE'),
  autocommit=True)
  # Grab the posted json
  postdata = json.loads(event['body'])
  # Make sure request type is included
  if 'request' in postdata or bool(postdata['request']):
    # Check if this is an upload
    if postdata['request'] == 'upload':
      # Validation of data
      if 'name' not in postdata or not bool(postdata['name']):
        res = {'status':'error','message':'name cannot be empty'}
      elif 'apikey' not in postdata or not bool(postdata['apikey']):
        res = {'status':'error','message':'apikey cannot be empty'}
      elif 'description64' not in postdata or not bool(postdata['description64']):
        res = {'status':'error','message':'description64 cannot be empty'}
      elif 'yaml64' not in postdata or not bool(postdata['yaml64']):
        res = {'status':'error','message':'yaml64 cannot be empty'}
      elif 'icon64' not in postdata or not bool(postdata['icon64']):
        res = {'status':'error','message':'icon64 cannot be empty'}
      # Looks like fields are present now need to validate them
      else:
        name = postdata['name']
        apikey = postdata['apikey']
        description64 = postdata['description64']
        yaml64 = postdata['yaml64']
        icon64 = postdata['icon64']
        # Check if user is authorized to upload and get their username
        cur = db.cursor()
        cur.execute('select name,guid from users where guid = %s',(apikey,))
        # Not a user reject
        if cur.rowcount == 0:
          res = {'status':'error','message':'unauthorized'}
        # User present in database
        else:
          try:
            taisunuser = cur.fetchone()
            username = taisunuser[0]
            stackyaml = base64.b64decode(yaml64)
            stackicon = base64.b64decode(icon64)
            stackguid = str(uuid.uuid4())
            # Make sure the stack template has the basics set
            yamltocheck = yaml.load(stackyaml)
            if 'name' not in yamltocheck or not bool(yamltocheck['name']) or 'description' not in yamltocheck or not bool(yamltocheck['description']) or 'form' not in yamltocheck or not bool(yamltocheck['form']) or 'compose' not in yamltocheck or not bool(yamltocheck['compose']):
              res = {'status':'error','message':'Stack Template missing core settings'}
            # Passed basic yaml sanity check
            else:
              s3 = boto3.client('s3')
              templateres = s3.put_object(Bucket=environ.get('STACKS_BUCKET'),Key='templates/' + stackguid + '.yml',Body=stackyaml)
              iconres = s3.put_object(Bucket=environ.get('STACKS_BUCKET'),Key='icons/' + stackguid + '.png',Body=stackicon)
              # Insert the metadata for the stack into the DB
              cur.execute('insert into stacks (guid,name,user,description) values (%s,%s,%s,%s)',(stackguid,name,username,description64))
              cur.close()
              res = {'status':'success','message':'Upload Completed for ' + name,"guid":stackguid}
          # Could not parse yaml
          except yaml.YAMLError:
            res = {'status':'error','message':'cannot parse yaml'}
          # Error with file upload
          except ClientError:
            res = {'status':'error','message':'could not upload files to AWS'}
          # Generic mysql error
          except pymysql.err.InternalError:
            res = {'status':'error','message':'Database Error'}
    # Check if this is a delete
    elif postdata['request'] == 'delete':
      # Validation of data
      if 'guid' not in postdata or not bool(postdata['guid']):
        res = {'status':'error','message':'guid cannot be empty'}
      elif 'apikey' not in postdata or not bool(postdata['apikey']):
        res = {'status':'error','message':'apikey cannot be empty'}
      # All variables present proceed
      else:
        apikey = postdata['apikey']
        stackguid = postdata['guid']
        # Check if user is authorized to delete and get their username
        cur = db.cursor()
        cur.execute('select name,guid from users where guid = %s',(apikey,))
        # Not a user reject
        if cur.rowcount == 0:
          res = {'status':'error','message':'unauthorized'}
        # User present in database
        else:
          taisunuser = cur.fetchone()
          username = taisunuser[0]
          cur.close()
          cur = db.cursor()
          cur.execute('select user from stacks where guid = %s',(stackguid,))
          # Not a stack reject
          if cur.rowcount == 0:
            res = {'status':'error','message':stackguid + ' is not a stack'}
          # Stack present in database
          else:
            userrow = cur.fetchone()
            dbusername = userrow[0]
            # This is the users stack
            if dbusername == username:
              try:
                s3 = boto3.client('s3')
                templateres = s3.delete_object(Bucket=environ.get('STACKS_BUCKET'),Key='templates/' + stackguid + '.yml')
                iconres = s3.delete_object(Bucket=environ.get('STACKS_BUCKET'),Key='icons/' + stackguid + '.png')              
                cur.execute('delete from stacks where guid = %s',(stackguid,))
                cur.close()
                res = {'status':'success','message':'Stack Deleted',"guid":stackguid}
              # Error with file upload
              except ClientError:
                res = {'status':'error','message':'could not upload files to AWS'}
              # Generic mysql error
              except pymysql.err.InternalError:
                res = {'status':'error','message':'Database Error'}
            else:
              res = {'status':'error','message':username + ' unauthorized to delete ' + stackguid}
    # Check if this is an edit
    elif postdata['request'] == 'edit':
      # Validation of data
      if 'guid' not in postdata or not bool(postdata['guid']):
        res = {'status':'error','message':'guid'}
      elif 'apikey' not in postdata or not bool(postdata['apikey']):
        res = {'status':'error','message':'apikey cannot be empty'}
      elif 'yaml64' not in postdata or not bool(postdata['yaml64']):
        res = {'status':'error','message':'yaml64 cannot be empty'}
      # Looks like fields are present now need to validate them
      else:
        yaml64 = postdata['yaml64']
        apikey = postdata['apikey']
        stackguid = postdata['guid']
        # Check if user is authorized to delete and get their username
        cur = db.cursor()
        cur.execute('select name,guid from users where guid = %s',(apikey,))
        # Not a user reject
        if cur.rowcount == 0:
          res = {'status':'error','message':'unauthorized'}
        # User present in database
        else:
          taisunuser = cur.fetchone()
          username = taisunuser[0]
          cur.close()
          cur = db.cursor()
          cur.execute('select user from stacks where guid = %s',(stackguid,))
          # Not a stack reject
          if cur.rowcount == 0:
            cur.close()
            res = {'status':'error','message':stackguid + ' is not a stack'}
          # Stack present in database
          else:
            userrow = cur.fetchone()
            dbusername = userrow[0]
            cur.close()
            # This is the users stack
            if dbusername == username:
              try:
                stackyaml = base64.b64decode(yaml64)
                # Make sure the stack template has the basics set
                yamltocheck = yaml.load(stackyaml)
                if 'name' not in yamltocheck or not bool(yamltocheck['name']) or 'description' not in yamltocheck or not bool(yamltocheck['description']) or 'form' not in yamltocheck or not bool(yamltocheck['form']) or 'compose' not in yamltocheck or not bool(yamltocheck['compose']):
                  res = {'status':'error','message':'Stack Template missing core settings'}
                # Passed basic yaml sanity check
                else:
                  s3 = boto3.client('s3')
                  templateres = s3.put_object(Bucket=environ.get('STACKS_BUCKET'),Key='templates/' + stackguid + '.yml',Body=stackyaml)
                  # Insert the metadata for the stack into the DB
                  res = {'status':'success','message':'Edit Completed','guid':stackguid}
              # Could not parse yaml
              except yaml.YAMLError:
                res = {'status':'error','message':'cannot parse yaml'}
              # Error with file upload
              except ClientError:
                res = {'status':'error','message':'could not upload files to AWS'}
            else:
              res = {'status':'error','message':username + ' unauthorized to edit ' + stackguid}
    # Request was not upload or delete    
    else:
      res = {'status':'error','message':'unkown request type not upload, delete, or edit'}
  else:
    res = {'status':'error','message':'request cannot be empty'}

  # Close mysql connection
  db.close()
  # Response
  responseCode = 200
  response = {
    'headers': {'Access-Control-Allow-Origin':'*'},
    'statusCode': responseCode,
    'body': json.dumps(res, indent=4, sort_keys=True)
  }
  return response
