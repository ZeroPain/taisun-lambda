from google.oauth2 import id_token
from google.auth.transport import requests
import json
import uuid
import boto3
from base64 import b64encode
from os import environ
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError
CLIENT_ID = environ['CLIENT_ID']

def lambda_handler(event, context):
  def validatetoken(token):
    try:
      idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
      if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
        return 'badauth'
      else:
        return idinfo['email']
    except ValueError:
      # Invalid token
      return 'badauth'  
  res = 'ok'
  # Check the google token passed by the user
  if event['queryStringParameters'] is not None:
    if 'token' in event['queryStringParameters']:
      sourceip = event['requestContext']['identity']['sourceIp']
      token = event['queryStringParameters']['token']
      email = validatetoken(token)
      # If this is a legit code and response grab the username
      if email == 'badauth':
        print('could not validate with google')
        res = 'badauth'
    else:
      res = 'badauth'
  else:
    res = 'badauth'
  # Check dynamodb for this users endpoint
  if res != 'badauth':
    dynamodb = boto3.resource("dynamodb", region_name='us-west-2')
    table = dynamodb.Table('dyndns')
    try:
      response = table.query(
          IndexName='email-index',
          KeyConditionExpression=Key('email').eq(email)
      )
    except ClientError as e:
      print(e)
      res = 'badauth'
    else:
      if 'Items' in response:
        if len(response['Items'])< 1:
          # Generate and create URL logic here
          try:
            dnskey = str(uuid.uuid4()).replace('-','')
            domain = str(uuid.uuid4()).replace('-','')
            response = table.put_item(
              Item={
                'guid': dnskey,
                'domain': domain,
                'ip': sourceip,
                'email': email
              }
            )
            ip = sourceip
            try:
              route53 = boto3.client('route53')
              response = route53.change_resource_record_sets(
                HostedZoneId = environ['ZONEID'],
                ChangeBatch = {
                  'Changes': [
                    {
                      'Action': 'CREATE',
                      'ResourceRecordSet': {
                        'Name': domain + environ['TLD'],
                        'Type': 'A',
                        'TTL': 60,
                        'ResourceRecords': [
                          {
                            'Value': ip
                          }
                        ]
                      }
                    }
                  ]
                }
              )
            except ClientError as e:
              print(e)
              res = 'badauth'
          except ClientError as e:
            print(e)
            res = 'badauth'
        else:
          # this allready exists just return the information
          item = response['Items'][0]
          domain = item['domain']
          dnskey = item['guid']
          ip = item['ip']
      else:
        print('item was not in resoponse')
        res = 'badauth'
  if res == 'badauth':
    responseCode = 200
    response = {
      'statusCode': responseCode,
      'body': json.dumps('{"error":"unauthorized"}', indent=4, sort_keys=True)
    }
    return response
  else:
    body = """
    <!DOCTYPE html>
    <html>
    <head>
      <title>Taisun DynDNS Information</title>
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" rel="stylesheet">
    </head>
      <body style="padding-top:40px;padding-bottom:40px;background-color:#eee;">
        <div class="card" style="max-width:800px;padding:15px;margin:0 auto;">
          <div class="card-block">
            <h2>Taisun DynDNS Information:</h2>
            <table class="table table-bordered">
              <tr><td>Domain</td><td>%(domain)s.taisun.io</td></tr>
              <tr><td>DNSKey</td><td>%(dnskey)s</td></tr>
              <tr><td>Update</td><td><a href="https://api.taisun.io/production/dyndns?key=%(dnskey)s" target="_blank">https://api.taisun.io/production/dyndns?key=%(dnskey)s</a></td></tr>
              <tr><td>Current IP</td><td>%(sourceip)s</td></tr>
            </table>\
            <p>Run updater in Docker:</p>
            <div class="card bg-faded">
              <pre>docker run --name taisundns -d -e DNSKEY=%(dnskey)s taisun/dyndns</pre>
            </div><br>
            <p>Add cron job to system:(requires curl)</p>
            <div class="card bg-faded">
              <pre>(crontab -l 2>/dev/null; echo "*/30 * * * * curl --insecure --silent https://api.taisun.io/production/dyndns?key=%(dnskey)s > /dev/null") | crontab -</pre>
            </div><br>
            <p>Manual curl cli:</p>
            <div class="card bg-faded">
              <pre>curl --insecure https://api.taisun.io/production/dyndns?key=%(dnskey)s</pre>
            </div><br>
            <p>Manual wget cli:</p>
            <div class="card bg-faded">
              <pre>wget --no-check-certificat -qO- https://api.taisun.io/production/dyndns?key=%(dnskey)s</pre>
            </div><br>
          </div>
        </div>
      </body>
    </html>
    """ % {'dnskey': dnskey,'domain': domain,'sourceip': ip}
    # Send the dns info to the client
    responseCode = 200
    response = {
      'headers':{
        'Content-Type': 'text/html'
      },
      'statusCode': responseCode,
      'body': body
    }
    return response
