from google.oauth2 import id_token
from google.auth.transport import requests
import json
import boto3
from os import environ
from boto3.dynamodb.conditions import Key, Attr
from botocore.exceptions import ClientError
CLIENT_ID = environ['CLIENT_ID']

def lambda_handler(event, context):
  def validatetoken(token):
    try:
      idinfo = id_token.verify_oauth2_token(token, requests.Request(), CLIENT_ID)
      if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
        return 'badauth'
      else:
        return idinfo['email']
    except ValueError:
      # Invalid token
      return 'badauth'  
  res = 'ok'
  # Check the google token passed by the user
  if event['queryStringParameters'] is not None:
    if 'token' in event['queryStringParameters']:
      token = event['queryStringParameters']['token']
      email = validatetoken(token)
      # If this is a legit code and response grab the username
      if email == 'badauth':
        print('could not validate with google')
        res = 'badauth'
    else:
      res = 'badauth'
  else:
    res = 'badauth'
  # Check dynamodb for this users endpoint
  if res != 'badauth':
    dynamodb = boto3.resource("dynamodb", region_name='us-west-2')
    table = dynamodb.Table('dyndns')
    try:
      response = table.query(
          IndexName='email-index',
          KeyConditionExpression=Key('email').eq(email)
      )
    except ClientError as e:
      print(e)
      res = 'badauth'
    else:
      if 'Items' in response:
        item = response['Items'][0]
        domain = item['domain']
        dnskey = item['guid']
      else:
        print('item was not in resoponse')
        res = 'badauth'
  if res == 'badauth':
    responseCode = 200
    response = {
      'statusCode': responseCode,
      'body': json.dumps('{"error":"unauthorized"}', indent=4, sort_keys=True)
    }
    return response
  else:
    # Send a redirect response so the JS client app for stacks.taisun.io can be used to manage stacks
    responseCode = 302
    response = {
      'statusCode': responseCode,
      'headers': {
          'Location': 'https://Taisun:' + dnskey + '@'+domain+'.taisun.io:4443/3f4349dd-54f9-46ab-bab4-33c6fad6a995'
      }
    }
    return response
