import json
import pymysql
import base64
import math
from os import environ

def lambda_handler(event, context):
  # Main Mysql Connect
  db=pymysql.connect(
  host=environ.get('MYSQL_URL'),
  user=environ.get('MYSQL_USER'),
  port=3306,
  passwd=environ.get('MYSQL_PASS'),
  db=environ.get('MYSQL_DATABASE'),
  autocommit=True)
  # Check if the page is a parameter being passed to the get request
  if event['queryStringParameters'] is not None:
    if 'page' in event['queryStringParameters']:
      page = event['queryStringParameters']['page']
      # If this is a whole integer then proceed
      if page is not '' and page.isdigit():
        offset = (int(page) * 25) - 25
        limit = 'limit 25 offset ' + str(offset)
        currentpage = page
      # Set to first page results
      else:
        limit = 'limit 25 offset 0'
    # Set to first page results
    else:
      limit = 'limit 25 offset 0'
  # Set to first page results
  else:
    limit = 'limit 25 offset 0'
  # Check if the search parameter is passed and build the query based on this
  if event['queryStringParameters'] is not None:
    if 'search' in event['queryStringParameters']:
      search = event['queryStringParameters']['search']
      # Make sure it is not empty and not malicious (just a string of letters wihtout spaces)
      if search is not '' and search.isalpha():
        query = "select sql_calc_found_rows * from stacks where name like '%" + search + "%' order by CAST(downloads AS int) desc " + limit
      # Do not search for anything
      else:
        query = "select sql_calc_found_rows * from stacks order by CAST(downloads AS int) desc " + limit
    # Do not search for anything
    else:
      query = "select sql_calc_found_rows * from stacks order by CAST(downloads AS int) desc " + limit
  # Do not search for anything
  else:
    query = "select sql_calc_found_rows * from stacks order by CAST(downloads AS int) desc " + limit
  # Set blank body array to add rows to later
  body = []
  # Execute Mysql call
  cur = db.cursor()
  cur.execute(query)
  results = cur.fetchall()
  # Get total results for the queary regardless of the limit we set
  cur.execute("select FOUND_ROWS()")
  totalrows = cur.fetchone()[0]
  # Close mysql connection
  cur.close()
  db.close()
  # Calculate total pages by dividing by 25 and always rounding up
  totalpages = math.ceil(int(totalrows)/25)
  # If the user set a page then send it back in the response
  if 'currentpage' in locals():
    pageresponse = {'num_pages':totalpages,'num_results':totalrows,'page_size':25,'page':int(currentpage)}
  else:
    pageresponse = {'num_pages':totalpages,'num_results':totalrows,'page_size':25,'page':1}
  # Build the stacks portion of the response from the mysql data
  for result in results:
    body.append(renderrow(result))
  responseCode = 200
  res = {'stacktemplates':body,'page':pageresponse}
  response = {
    'headers': {'Access-Control-Allow-Origin':'*'},
    'statusCode': responseCode,
    'body': json.dumps(res, indent=4, sort_keys=True)
  }
  return response
  
def renderrow(row):
  result = {}
  result['guid'] = row[0]
  result['name'] = row[1]
  result['user'] = row[2]
  # The user description is stored in base64 need to decode it
  result['description'] = base64.b64decode(row[3]).decode('ascii')
  result['downloads'] = row[4]
  # The icon and stack yml URL are based on the guid of the entry
  result['icon'] = 'https://stacks.taisun.io/icons/' + row[0] + '.png'
  result['stackdata'] = 'https://stacks.taisun.io/templates/' + row[0] + '.yml'
  return result
