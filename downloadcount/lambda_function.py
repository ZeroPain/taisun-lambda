import json
import pymysql
from pymysql.constants import ER
from os import environ
def lambda_handler(event, context):
  # Main Mysql Connect
  db=pymysql.connect(
  host=environ.get('MYSQL_URL'),
  user=environ.get('MYSQL_USER'),
  port=3306,
  passwd=environ.get('MYSQL_PASS'),
  db=environ.get('MYSQL_DATABASE'),
  autocommit=True)
  # Check if the guid parameter is in the request
  if event['queryStringParameters'] is not None:
    if 'guid' in event['queryStringParameters']:
      guid = event['queryStringParameters']['guid']
      try:
        query = 'UPDATE stacks SET downloads = downloads + 1 WHERE guid = %s'
        cur = db.cursor()
        cur.execute(query,(guid,))
        cur.close()
        # Just send peopel an Ok even if there were zero rows modified
        res = {'status':'success','message':'updated download count for ' + guid}
      # Generic mysql error
      except pymysql.err.InternalError:
        res = {'status':'error','message':'Database Error'}
    # The guid was not a string parameter    
    else:
      res = {'status':'error','message':'guid is required to update the stack in question'}
  # The guid was not a string parameter
  else: 
    res = {'status':'error','message':'guid is required to update the stack in question'}
  # Close DB connection
  db.close()
  # Response
  responseCode = 200
  response = {
    'headers': {'Access-Control-Allow-Origin':'*'},
    'statusCode': responseCode,
    'body': json.dumps(res, indent=4, sort_keys=True)
  }
  return response
